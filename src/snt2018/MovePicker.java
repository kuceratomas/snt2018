package snt2018;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MovePicker {

	private XMLData xmlData;
	private List<DayOfWeekCover> days;
	private int[][][] recency;
	private int tabuTenure;
	Random r;
	
	public MovePicker(XMLData xmlData, List<DayOfWeekCover> days)
	{
		this.xmlData = xmlData;
		this.days = days;
		
		r = new Random();
		this.tabuTenure = (int) (0.8 * xmlData.employees.size() + (1 + (3 - 1) * r.nextDouble()));
		recency = new int[days.size()][xmlData.employees.size()][xmlData.shifts.size()];
		for (int[][] row : recency)
			for (int[] col : row)
				Arrays.fill(col, -1);
		
	}

	/************** PART 3: DIVERSIFICATION SEARCH *****************/
	public Move diversificationSearch(int Xprice, Shift[][] SDMatrix, List<Integer> subset, List<List<Move>> moves, boolean setOfMoves) throws CloneNotSupportedException
	{
		//System.out.println("diver");
		List<Move> subPromisingMoves = new ArrayList<Move>();
		List<Move> nurseMoves = new ArrayList<Move>();

		for (List<Move> dayMoves : moves)
		{
			for (Move m : dayMoves)
			{
				if (Utils.bothNursesFound(m.nurse1, m.nurse2, subset)) // obe sestry z Move su v subsete
				{
					nurseMoves.add(m);
					int currentPrice = Utils.getMovePrice(m, SDMatrix, xmlData, days);
					
					if (currentPrice < Xprice) // move ma lepsi price ako f(X)
					{
						subPromisingMoves.add(m);
					}
				}
			}
		}
		
		if (subPromisingMoves.size() > 0) // nejake tahy lepsie ako terajsi -> random vybrat jeden
		{
			Collections.shuffle(subPromisingMoves);
			return subPromisingMoves.get(0);
		}
		
		if (nurseMoves.size() > 0) // nejake tahy lepsie ako terajsi -> random vybrat jeden
		{
			Collections.shuffle(nurseMoves); // ziadne lepsie nie su -> random vybrat z tahov tykajucich sa danych sestier
			return nurseMoves.get(0);
		}
		
		Collections.shuffle(moves);
		Collections.shuffle(moves.get(0));
		return moves.get(0).get(0);
		
	
	}
	
	/************** PART 2: INTERMEDIATE SEARCH *****************/
	public Move intermediateSearch(Shift[][] SDMatrix, List<Integer> subset, List<List<Move>> moves, boolean setOfMoves, int iter) throws CloneNotSupportedException
	{
		//System.out.println("inter");
		int bestPrice = Integer.MAX_VALUE;
		Move bm = null, sbm = null;

		for (List<Move> dayMoves : moves)
		{
			for (Move m : dayMoves)
			{
				if (Utils.bothNursesFound(m.nurse1, m.nurse2, subset)) // obe sestry z Move su v subsete
				{
					int currentPrice =  Utils.getMovePrice(m, SDMatrix, xmlData, days);
					
					if (currentPrice < bestPrice) // ak je lepsi ako best so far local -> je to best move
					{
						bestPrice = currentPrice;
						sbm = bm;
						bm = m;
					}
				}
			}
		}
		
		if (sbm != null)
		{
			if (((recency[bm.day][bm.nurse2][bm.shift1] > -1 && iter == recency[bm.day][bm.nurse2][bm.shift1] + 1) || // sestre 2 bola odobrana sluzba 1 v rozmedzi tenure iteracii
					setOfMoves && (recency[bm.day][bm.nurse1][bm.shift2] > -1 && iter == recency[bm.day][bm.nurse1][bm.shift2] + 1)) && r.nextDouble() < 0.5) // ak sa menia obe -> kontrola ci sestre 1 nebola odobrana sluzba 2

			{
				recency[sbm.day][sbm.nurse1][sbm.shift1] = iter; // odoberam 1. sestre 1. sluzbu vzdy
				
				if (setOfMoves)
					recency[sbm.day][sbm.nurse2][sbm.shift2] = iter; // ak je to obojsmerna vymena -> odoberam 2. sestre 2. sluzbu
				
				return sbm; // vrati sa druhe najlepsie riesenie - prve bolo vykonane minulu iteraciu
			}
		}
		
		if (bm != null)
		{
			recency[bm.day][bm.nurse1][bm.shift1] = iter; // odoberam 1. sestre 1. sluzbu vzdy
			
			if (setOfMoves)
				recency[bm.day][bm.nurse2][bm.shift2] = iter; // ak je to obojsmerna vymena -> odoberam 2. sestre 2. sluzbu
			
			return bm;
		}
		//System.out.print("----------------------------------------");
		
		return moves.get(0).get(0);
		
	}
	
	/************** PART 1: INTENSIVE SEARCH *****************/
	public Move intensiveSearch(Shift[][] SDMatrix, int XStarprice, List<List<Move>> moves, boolean setOfMoves, int iter) throws CloneNotSupportedException
	{
		//System.out.println("intensive");
		int bestPrice = Integer.MAX_VALUE;
		Move bm = null;
		// prehladanie celeho priestoru trva dlho -> optimalizovat?
		for (List<Move> dayMoves : moves)
		{
			for (Move m : dayMoves)
			{
				int currentPrice =  Utils.getMovePrice(m, SDMatrix, xmlData, days);
				
				if ((recency[m.day][m.nurse2][m.shift1] > -1 && iter - recency[m.day][m.nurse2][m.shift1] < tabuTenure) || // sestre 2 bola odobrana sluzba 1 v rozmedzi tenure iteracii
						(setOfMoves && (recency[m.day][m.nurse1][m.shift2] > -1 && iter - recency[m.day][m.nurse1][m.shift2] < tabuTenure))) // ak sa menia obe -> kontrola ci sestre 1 nebola odobrana sluzba 2
				{
					if (currentPrice < bestPrice && currentPrice < XStarprice) // povolenie swapu z tabuList iba ak je to best so far riesenie
					{
						bestPrice = currentPrice;
						bm = m;
					}
				}
				else if (currentPrice < bestPrice) // swap nebol vykonany tabuterure iteracii -> ak je lepsi ako best so far local -> je to best move
				{
					bestPrice = currentPrice;
					bm = m;
				}
			}
		}
			
		if (bm != null)
		{
			recency[bm.day][bm.nurse1][bm.shift1] = iter; // odoberam 1. sestre 1. sluzbu vzdy
			
			if (setOfMoves)
				recency[bm.day][bm.nurse2][bm.shift2] = iter; // ak je to obojsmerna vymena -> odoberam 2. sestre 2. sluzbu
			
			return bm;
		}
		//System.out.print("----------------------------------------");
		
		return moves.get(0).get(0);
	}
	
	
	

}
