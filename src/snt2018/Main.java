package snt2018;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

public class Main
{

	public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException, CloneNotSupportedException, ParseException, TransformerFactoryConfigurationError, TransformerException
	{
		// spracovanie parametrov
		if (args.length < 2)
		{
			System.err.println("Wrong arguments!\n"
					+ "Usage: make=args\"TIME XMLFile\" run\n"
					+ "TIME     - required time for the computation [s]\n"
					+ "XMLFile  - input xml file");
			System.exit(1);
		}
		
		String fileName = args[1];
		
		XMLData xmlData = XMLParser.parse(fileName);
		
		// zistenie dlzky obdobia
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date1 = LocalDate.parse(xmlData.seDate.startDate, formatter);
		LocalDate date2 = LocalDate.parse(xmlData.seDate.endDate, formatter);
		int dayCnt = (int) ChronoUnit.DAYS.between(date1,date2) + 1;
		int startDay = date1.getDayOfWeek().getValue() - 1;
		
		// inicializacia rozvrhoveho obdobia
		List<DayOfWeekCover> days = new ArrayList<DayOfWeekCover>();
		for (int i = 0; i < dayCnt; i++) // inicializacia mesiaca
		{
			days.add(xmlData.dowCovers.get((startDay + i) % 7).clone());
			DayOfWeekCover day = days.get(days.size()-1);
			day.date = date1.format(formatter);
			day.dayFromDate = date1.getDayOfMonth();
			date1 = date1.plusDays(1);
		}
		
		// zapocatie algoritmu
		long time = Integer.parseInt(args[0]);
		long startTime = System.currentTimeMillis();
		long endTime = startTime + time*1000; // 60 seconds * 1000 ms/sec
		
		ANS ans = new ANS(xmlData, startTime, endTime, fileName, days);
		Shift[][] finalRoster = ans.ansAlg();
		//System.out.println(PriceEvaluator.getPrice(finalRoster, xmlData, days));
		
		// finalny vypis
		ResultPrinter.printOut(finalRoster, days, xmlData, fileName);
	}
	

}

