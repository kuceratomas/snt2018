package snt2018;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PriceEvaluator {
	
	public enum Con
	{
		SAPD(0), MaxNA(1), MinNA(2), MaxCWD(3), MinCWD(4), MaxCFD(5), MinCFD(6), MaxCWW(7),
		MinCWW(8), MaxNWW(9), WDef(10), CW(11), ISTDW(12), NNSBFW(13), ASC(14);
		
		private int i;
		
		Con(final int i) {
			this.i = i;
		}
		
		public int getI() {
			return i;
		}
	};
	
	/************** VYHODNOTENIE CENY RIESENIA *****************/
	public static int getPrice(Shift[][] roster, XMLData xmlData, List<DayOfWeekCover> days)
	{
		// vypocitanie ceny riesenia roster;
		int price = 0;
		for (int i = 0; i < roster[0].length; i++)
		{
			Employee emp = xmlData.employees.get(i);
			
			SoftConstr NA1 = emp.contract.constr.get(Con.MaxNA.getI());
			SoftConstr NA2 = emp.contract.constr.get(Con.MinNA.getI());
			SoftConstr CWD1 = emp.contract.constr.get(Con.MaxCWD.getI());
			SoftConstr CWD2 = emp.contract.constr.get(Con.MinCWD.getI());
			SoftConstr CFD1 = emp.contract.constr.get(Con.MaxCFD.getI());
			SoftConstr CFD2 = emp.contract.constr.get(Con.MinCFD.getI());
			SoftConstr CWW1 = emp.contract.constr.get(Con.MaxCWW.getI());
			SoftConstr CWW2 = emp.contract.constr.get(Con.MinCWW.getI());
			SoftConstr NWW = emp.contract.constr.get(Con.MaxNWW.getI());
			
			int maxNA = NA1.value;
			int minNA = NA2.value;
			int maxCWD = CWD1.value;
			int minCWD = CWD2.value;
			int maxCFD = CFD1.value;
			int minCFD = CFD2.value;
			int maxCWW = CWW1.value;
			int minCWW = CWW2.value;
			int maxNWW = NWW.value;
			
			int NA = 0;
			int lenWksect = 0;
			int lenWkdsect = 0;
			boolean workingWeekend = false;
			boolean isWeekend = false;
			int[] cw = { 0, 0, 0, 0};
			Map<String, Integer> sTypes = new HashMap<String, Integer>();
			int dayOfWeekend = 0;
			int nwd = 0;
			int lenFrsect = 0;
			int fromNight = days.size();
			int workingDaysInWeekend = 0;
			int skillWeight = emp.contract.constr.get(Con.ASC.getI()).weight;
			
			int s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s14,s16,s17,s18;
			s1=s2=s3=s4=s5=s6=s7=s8=s9=s10=s11=s12=s14=s16=s17=s18=0;
			
			for (int j = 0; j < roster.length; j++)
			{
				DayOfWeekCover currentDay = days.get(j);
				
				
				if (roster[j][i] != null) // PRACUJE
				{
					NA++;
					lenWksect++;
					
					// s14
					if (emp.daysOff.containsKey(currentDay.date))
							s14 += emp.daysOff.get(currentDay.date).weight;
					
					// s16
					if (emp.shiftsOff.containsKey(roster[j][i].ID + currentDay.date))
						s14 += emp.shiftsOff.get(roster[j][i].ID + currentDay.date).weight;
					
					// s17
					if (skillWeight > 0 && !emp.skills.containsAll(roster[j][i].skills))
						s17++;
					
					// s18
					for (Pattern p : emp.contract.uPatterns)
					{
						if (j + p.pEntries.size() <= days.size())// kontrolujeme po j-size pattern entry den
						{
							boolean patternExists = true; // predpoklad, ze pattern existuje
							for (int k = 0; k < p.pEntries.size(); k++)
							{
								PatternEntry pEntry = p.pEntries.get(k);
								if (roster[j+k][i] == null) // v dany den nie je sluzba
								{
									if (!pEntry.sType.equals("None")) // v patterne nebolo none -> pattern poruseny
									{
										patternExists = false;
										break;
									}
									continue; // v patterne bolo none -> pattern stale plati
								}
								if ((!pEntry.sType.equals(roster[j + k][i].ID) && !pEntry.sType.equals("Any")) ||
									((pEntry.day != 7) && days.get(j + k).day!= pEntry.day))
								{ // nesedi den alebo shift type -> neplati{
									patternExists = false;
									break;
								}
							}
							if (patternExists) s18 += p.weight;
						}
					}
					
					
					if (emp.weekend.contains(currentDay.day)) // je vikend
					{
						cw[dayOfWeekend] = 1;
						workingDaysInWeekend++;
						
						if (sTypes.containsKey(roster[j][i].ID))
				        {
				            Integer value = sTypes.get(roster[j][i].ID);
				            value++;
				            sTypes.put(roster[j][i].ID, value);
				        }
						else
						{
							sTypes.put(roster[j][i].ID, 1);
						}
						
						dayOfWeekend++;
						
						if (!workingWeekend)
						{
							lenWkdsect++;
							nwd++;
							workingWeekend = true;
						}
						
						isWeekend = true;
					}
					else // nie je vikend
					{
						if (isWeekend) // akurat skoncil vikend
						{
							if (!workingWeekend) // bol cely vikend nepracujuci -> spocitat predosle pracujuce
							{
								s8 += Math.max(lenWkdsect - maxCWW, 0); // s8
								if (lenWkdsect > 0)
									s9 += Math.max(minCWW - lenWkdsect, 0); // s9
								
								lenWkdsect = 0; // ukoncenie retaze pracujucich vikendov
							}
							workingWeekend = false;
							isWeekend = false;
							
							// s11
							if ((emp.weekend.size() == 3) && cw[0] == 1 && cw[1] == 0 &&cw[2] == 0)
								s11 += 4;
							else if (workingDaysInWeekend > 0 && workingDaysInWeekend < emp.weekend.size())
								s11 += (emp.weekend.size() - workingDaysInWeekend);
							
							// s12
							if (workingDaysInWeekend == emp.weekend.size())
							{
								for (Integer value : sTypes.values())
									s12 += (emp.weekend.size() - value);

							}
							sTypes.clear();
							dayOfWeekend = 0;
							workingDaysInWeekend = 0;
							cw[0] = 0; cw[1] = 0; cw[2] = 0; cw[3] = 0;
						}
						
					}

					if (roster[j][i].desc.equals("Night"))
					{
						fromNight = 0;
					}
					else
					{
						if (fromNight < 2) s7++;
						fromNight++;
					}
					
					
					s5 += Math.max(lenFrsect - maxCFD, 0); // s5
					if (lenFrsect > 0)
						s6 += Math.max(minCFD - lenFrsect, 0); // s6
					

					
					if (j == days.size() - 1) // posledny den treba spocitat pracovne dni, vikendy, complete weekends
					{
						s3 += Math.max(lenWksect - maxCWD, 0); // s3
						if (lenWksect > 0)
							s4 += Math.max(minCWD - lenWksect, 0); // s4
						
						s8 += Math.max(lenWkdsect - maxCWW, 0); // s8
						if (lenWkdsect > 0)
							s9 += Math.max(minCWW - lenWkdsect, 0); // s9
						
						// s11
						if ((emp.weekend.size() == 3) && cw[0] == 1 && cw[1] == 0 &&cw[2] == 0)
							s11 += 4;
						else if (workingDaysInWeekend > 0 && workingDaysInWeekend < dayOfWeekend)  // zmenene dayOfWeekend na zaklade toho kolko dni z vikendu ubehlo
							s11 += (dayOfWeekend - workingDaysInWeekend);
						
						
						// s12
						if (workingDaysInWeekend == emp.weekend.size())
						{
							for (Integer value : sTypes.values())
								s12 += (emp.weekend.size() - value);

						}
					}
					
					lenFrsect = 0;
				}
				else // NEPRACUJE
				{
					lenFrsect++;
					fromNight++;
					
					
					// s18
					for (Pattern p : emp.contract.uPatterns)
					{
						if (j + p.pEntries.size() <= days.size())// kontrolujeme po j-size pattern entry den
						{
							boolean patternExists = true; // predpoklad, ze pattern existuje
							for (int k = 0; k < p.pEntries.size(); k++)
							{
								PatternEntry pEntry = p.pEntries.get(k);
								if (roster[j + k][i] == null) // v dany den nie je sluzba
								{
									if (!pEntry.sType.equals("None")) // v patterne nebolo none -> pattern poruseny
									{
										patternExists = false;
										break;
									}// v patterne bolo none -> pattern stale plati -> dalsia iteracia
								}
								else if ((!pEntry.sType.equals(roster[j + k][i].ID) && !pEntry.sType.equals("Any")) ||
										((pEntry.day != 7) && days.get(j + k).day != pEntry.day))
								{ // PRACUJE a nesedi den alebo shift type -> neplati{
									patternExists = false;
									break;
								}
							}
							if (patternExists) s18 += p.weight;
						}
					}
					
					
					if (emp.weekend.contains(currentDay.day)) // je vikend
					{
						cw[dayOfWeekend] = 0;
						dayOfWeekend++;
						isWeekend = true;
					}
					else // nie je vikend
					{
						if (isWeekend) // akurat skoncil vikend
						{
							if (!workingWeekend) // bol cely vikend nepracujuci -> spocitat predosle pracujuce
							{
									s8 += Math.max(lenWkdsect - maxCWW, 0); // s8
									if (lenWkdsect > 0)
										s9 += Math.max(minCWW - lenWkdsect, 0); // s9
								
								lenWkdsect = 0; // ukoncenie retaze pracujucich vikendov
							}
							workingWeekend = false;
							
							// s11
							if ((emp.weekend.size() == 3) && cw[0] == 1 && cw[1] == 0 &&cw[2] == 0)
								s11 += 4;
							else if (workingDaysInWeekend > 0 && workingDaysInWeekend < emp.weekend.size())
								s11 += (emp.weekend.size() - workingDaysInWeekend);
							
							// s12
							if (workingDaysInWeekend == emp.weekend.size())
							{
								for (Integer value : sTypes.values())
									s12 += (emp.weekend.size() - value);

							}
							sTypes.clear();
							dayOfWeekend = 0;
							workingDaysInWeekend = 0;
							cw[0] = 0; cw[1] = 0; cw[2] = 0; cw[3] = 0;
						}
						
					}
					
					// co ak to je posledny den (enddate)
					s3 += Math.max(lenWksect - maxCWD, 0); // s3
					if (lenWksect > 0)
						s4 += Math.max(minCWD - lenWksect, 0); // s4
					

					
					if (j == days.size() - 1) // ak je to posledny den - treba kontrolovat nepracovne dni
					{
						s5 += Math.max(lenFrsect - maxCFD, 0); // s5
						if (lenFrsect > 0)
							s6 += Math.max(minCFD - lenFrsect, 0); // s6
						
						s8 += Math.max(lenWkdsect - maxCWW, 0); // s8
						if (lenWkdsect > 0)
							s9 += Math.max(minCWW - lenWkdsect, 0); // s9
						
						// s11
						if ((emp.weekend.size() == 3) && cw[0] == 1 && cw[1] == 0 &&cw[2] == 0)
							s11 += 4;
						else if (workingDaysInWeekend > 0 && workingDaysInWeekend < dayOfWeekend) // zmenene dayOfWeekend na zaklade toho kolko dni z vikendu ubehlo
							s11 += (dayOfWeekend - workingDaysInWeekend);
						
						// s12
						if (workingDaysInWeekend == emp.weekend.size())
						{
							for (Integer value : sTypes.values())
								s12 += (emp.weekend.size() - value);

						}
					}
					
					lenWksect = 0;
				}
			}
				
			
			
			s1 = Math.max(NA - maxNA, 0); // s1
			if (NA > 0)
				s2 = Math.max(minNA - NA, 0); // s2
			
			s10 = Math.max(nwd - maxNWW, 0); // s10
			
			//System.out.println(i + "--------------------------------------------------------------------------");
			
			s1 *= NA1.weight;
			s2 *= NA2.weight;
			s3 *= CWD1.weight;
			s4 *= CWD2.weight;
			s5 *= CFD1.weight;
			s6 *= CFD2.weight;
			// s7 nema vahu
			s8 *= CWW1.weight;
			s9 *= CWW1.weight;
			s10 *= NWW.weight;
			s11 *= emp.contract.constr.get(Con.CW.getI()).weight;
			s12 *= emp.contract.constr.get(Con.ISTDW.getI()).weight;
			//price += s14; // s14-s18 vahy pri jednotlivych instanciach
			//price += s16;
			//price += s17;
			//price += s18;
			
			
			price += s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8 + s9 + s10 + s11 + s12 + s14 + s16 + s17 + s18;
			
			//System.out.println(price + "\n1 " + s1 + "\n2 " + s2 + "\n3 " + s3 + "\n4 " + s4 + "\n5 " + s5 + "\n6 " + s6 + "\n7 " + s7 + "\n8 " + s8 + "\n9 " + s9 + "\n10 " + s10 + "\n11 " + s11 + "\n12 " + s12 + "\n14 " + s14 + "\n16 " + s16 + "\n17 " + s17 + "\n18 " + s18);
			
		}
		
		return price;
	}
	
}
