package snt2018;

import java.util.List;

public class Utils {

	/************** APLIKOVANIE TAHU NA RIESENIE *****************/
	public static Shift[][] applyMove(Move move, Shift[][] roster) throws CloneNotSupportedException
	{
		Shift s1 = null, s2 = null;
		if (roster[move.day][move.nurse1] != null) s1 = roster[move.day][move.nurse1].clone();
		if (roster[move.day][move.nurse2] != null) s2 = roster[move.day][move.nurse2].clone();

		roster[move.day][move.nurse2] = s1;
		roster[move.day][move.nurse1] = s2;
		
		return roster;
	}
	
	/************** ZISTENIE CENY TAHU BEZ SAMOTNEHO APLIKOVANIA *****************/
	public static int getMovePrice(Move move, Shift[][] roster, XMLData xmlData, List<DayOfWeekCover> days) throws CloneNotSupportedException
	{
		Shift s1 = null, s2 = null;
		if (roster[move.day][move.nurse1] != null) s1 = roster[move.day][move.nurse1].clone();
		if (roster[move.day][move.nurse2] != null) s2 = roster[move.day][move.nurse2].clone();
		
		roster[move.day][move.nurse2] = s1;
		roster[move.day][move.nurse1] = s2; // obojstranna vymena
		
		int price = PriceEvaluator.getPrice(roster, xmlData, days);
		
		roster[move.day][move.nurse1] = s1;
		roster[move.day][move.nurse2] = s2;	
		
		return price;
	}
	
	/************** TYKA SA TAH OBOCH SESTIER *****************/
	public static boolean bothNursesFound(int nurse1, int nurse2, List<Integer> subset)
	{
		boolean found = false;
		
		for (int i : subset)
		{
			if (nurse1 == i || nurse2 == i)
			{
				if (!found)
					found = true;
				else
					return true;
			}
		}
		return false;
		
	}
	
	/************** KOPIA POLA *****************/
	public static Shift[][] copyArray(Shift[][] original) throws CloneNotSupportedException
	{
		Shift[][] copy = new Shift[original.length][original[0].length];
		for (int i = 0; i < original.length; i++)
		{
			for (int j = 0; j < original[0].length; j++)
			{
				if (original[i][j] != null)
					copy[i][j] = original[i][j].clone();
			}
		}
		return copy;
	}
}
