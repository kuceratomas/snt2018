package snt2018;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ResultPrinter {
	
	/************** VYPIS DO XML *****************/
	public static void printOut(Shift[][] roster, List<DayOfWeekCover> days, XMLData xmlData, String fileName) throws ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError, TransformerException
	{
		File f = new File(fileName);
		fileName = f.getName();
		
		if (fileName.endsWith(".xml"))
			fileName = fileName.substring(0, fileName.length()-4);
		
		String output = "<Solution>";
		output += "<SchedulingPeriodID>" + fileName + "</SchedulingPeriodID>";
		output += "<Competitor>xkucer90</Competitor>";
		output += "<SoftConstraintsPenalty>1</SoftConstraintsPenalty>";

		for (int i = 0; i < days.size(); i++)
		{
			for (int j = 0; j < roster[i].length; j++)
			{
				if (roster[i][j] != null)
				{
					String date = days.get(i).date;
					String employee = xmlData.employees.get(j).ID;
					String shift = roster[i][j].ID;
					output += "<Assignment><Date>" + date + "</Date><Employee>" + employee + "</Employee><ShiftType>" + shift + "</ShiftType></Assignment>";
				}
				
			}
		}
		output += "</Solution>";
		
		// vytvorenie pretty xml
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();;
        Document doc = builder.parse(new InputSource(new StringReader(output)));

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = result.getWriter().toString();
        
        // zapis do suboru
        String path = f.getAbsolutePath();
        String outputPath = path.substring(0, path.length() - 4) + "sol" + path.substring(path.length() - 4);
        PrintWriter out = new PrintWriter(outputPath);
        out.println(xmlString);
        out.close();
        
        System.out.println("\n--- An output file created: " + outputPath);
	}
}
