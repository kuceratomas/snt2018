package snt2018;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.xml.sax.SAXException;

public class ANS
{
	private long endTime;
	private long startTime;
	private XMLData xmlData;
	private List<DayOfWeekCover> days;
	private  int X0price, Xlprice, Xprice, XStarprice;
	private MovePicker mPicker;
	Random r;
	
	private Shift SDMatrix[][];
	
	public ANS(XMLData xmlData, long startTime, long endTime, String fileName, List<DayOfWeekCover> days) throws CloneNotSupportedException, ParseException, ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError, TransformerException
	{
		this.xmlData = xmlData;
		this.endTime = endTime;
		this.startTime = startTime;
		this.days = days;
		r = new Random();
		
		SDMatrix = new Shift[days.size()][xmlData.employees.size()];	

		initialSolution();
		
		mPicker = new MovePicker(xmlData, days);

	}
	
	/************** VYTVORENIE POCIATOCNEHO RIESENIA *****************/
	public void initialSolution() throws CloneNotSupportedException
	{
		int l = 0;
		int empSize = xmlData.employees.size();
		for (int i = 0; i < days.size(); i++) // vsetky dni
		{
			List<Employee> employees = new ArrayList<Employee>(xmlData.employees);
			Collections.shuffle(employees);
			for (int j = 0; j < days.get(i).covers.size(); j++) // vsetky zmeny
			{
				for (int k = 0; k < days.get(i).covers.get(j).preferred; k++) // pokym sa smena nenaplni
				{
					SDMatrix[i][employees.get(l++).index] = days.get(i).covers.get(j).shift; 
					if (l == empSize) // kruhove priradenie zamestnancov
						l = 0;
				}
			}
		}
	}
	
	/************** JADRO ANS ALGORITMU *****************/
	public Shift[][] ansAlg() throws CloneNotSupportedException
	{
		Shift[][] SDMatrix0 = Utils.copyArray(SDMatrix), SDMatrixStar = Utils.copyArray(SDMatrix); // initial solution and best so far
		X0price = XStarprice = PriceEvaluator.getPrice(SDMatrix, xmlData, days);

		double dl = 0.0;
		int bestPriceSoFar = X0price;
		int adap_f = XStarprice;
		int treshold = xmlData.employees.size() * xmlData.shifts.size() / 10;
		
		int totalPreferred = 0; // suma sc(d,h) pre spocitanie q
		for (DayOfWeekCover dowc : days)
			for (Cover c : dowc.covers)
				totalPreferred += c.preferred;
				
		double q = 1.0 - 0.4 * (totalPreferred / (xmlData.employees.size() * (double)days.size()));
		int i = 0;
		do
		{
			SDMatrix = Utils.copyArray(SDMatrix0); // current solution
			Shift[][] SDMatrixl = Utils.copyArray(SDMatrix0); // best solution in current local search round
			Xprice = Xlprice = X0price;

			int adap_iter = 0;
			Move prevMove = null;
			Move chosenMove = null;
			List<List<List<Move>>> moves = null;
			
			int j = 0;
			while (j < 500) // improvement cutoff 200 naj
			{
				
				// find and apply the best move
				
				moves = getFeasibleMoves2(prevMove, moves);
				// vyber z M1 alebo M2 na zaklade parametru q
				if (r.nextDouble() < q) // vymenit, davalo lepsie vysledky????????
					chosenMove = findMove(dl, moves.get(0), false, i);
				else
					chosenMove = findMove(dl, moves.get(1), true, i);
				
				prevMove = chosenMove;
				SDMatrix = Utils.applyMove(chosenMove, SDMatrix);

				Xprice = PriceEvaluator.getPrice(SDMatrix, xmlData, days);
				if (Xprice < Xlprice) // if currently found is better than currently best local
				{
					SDMatrixl = Utils.copyArray(SDMatrix);
					Xlprice = Xprice;
					j = 0;
					//System.out.println("New best local solution found");
				}
				else
				{
					j++;
				}
				long currentTime = System.currentTimeMillis();
				
				if (Xprice < bestPriceSoFar)
				{
					bestPriceSoFar = Xprice;
					System.out.println("New best solution - iter: " + i + " , time: " + (double)(currentTime-startTime)/1000 + " s");
				}
				
				if (currentTime > endTime)
					break;

				
				//System.out.println(i + " " + j + " " + Xprice);
				// upravenie dl parametru
				if (i - adap_iter > treshold)
				{
					dl += (1.0 - dl) / 6.0;
					adap_f = Xprice;
					adap_iter = i;
				}
				else if (Xprice < adap_f)
				{
					dl -= dl / 10.0;
					adap_f = Xprice;
					adap_iter = i;
				}
				
				i++;
				//if ((System.currentTimeMillis() > endTime)) break;

			}

			if (Xlprice < XStarprice) // if current local best is better than global best
			{
				SDMatrixStar = Utils.copyArray(SDMatrixl);
				XStarprice = Xlprice;
				//System.out.println("New best global solution found");
			}
			
			
			if (r.nextDouble() < 0.5)  // restart search from an elite solution
			{
				SDMatrix0 = Utils.copyArray(SDMatrixl);
				X0price = Xlprice;
			}
				
			else
			{
				SDMatrix0 = Utils.copyArray(SDMatrixStar);
				X0price = XStarprice;
			}
				
			
			dl = 1.0;

		} while (System.currentTimeMillis() < endTime); // casovac
		
		//PriceEvaluator.getPrice(SDMatrixStar, xmlData, days);
		//System.out.println( "best solution:" + XStarprice + " in iter: " + bestIter);

		
		return SDMatrixStar;
	}
	
	/************** NAJDENIE NAJLEPSIEHO TAHU POMOCOU MOVEPICKER *****************/
	private Move findMove(double dl, List<List<Move>> moves, boolean setOfMoves, int i) throws CloneNotSupportedException
	{
		Move bestMove = null;
		double b1 = 0.35, b2 = 0.75;
		
		if (dl < b1) // intensive search
		{
			bestMove = mPicker.intensiveSearch(SDMatrix, XStarprice, moves, setOfMoves, i);
		}
		else if (dl < b2) // intermediate search
		{
			List<Integer> subset = getNursesSubset();
			bestMove = mPicker.intermediateSearch(SDMatrix, subset, moves, setOfMoves, i);
		}
		else // diversification search
		{
			List<Integer> subset = getNursesSubset();
			bestMove = mPicker.diversificationSearch(Xprice, SDMatrix, subset, moves, setOfMoves);
		}
		
		
		return bestMove;

	}

	
	/************** ZISKANIE PODMNOZINY SESTRICIEK *****************/
	private List<Integer> getNursesSubset()
	{
		List<Integer> list = new ArrayList<Integer>();
		List<Integer> subset = new ArrayList<Integer>();
		for (int i = 0; i < xmlData.employees.size(); i++)
		{
			list.add(i);
		}
		Collections.shuffle(list);
		for (int i = 0; i < xmlData.employees.size()/2; i++)
		{
			subset.add(list.get(i));
		}
		 
		 return subset;
	}

	/************** ZISKANIE USKUTOCNITELNYCH TAHOV V STREDE VYPOCTU - OPTIMALIZACIA *****************/
	private List<List<List<Move>>> getFeasibleMoves2(Move prevMove, List<List<List<Move>>> moves)
	{
		if (moves == null) // prvy beh cyklu -> inicializacia moznych tahov
		{
			return getFeasibleMoves();
		}
		
		if (prevMove != null) // pre posledny tah sa vymazu dni a prepocitaju sa nove
		{
			moves.get(0).remove(prevMove.day);
			moves.get(1).remove(prevMove.day);
			moves.get(0).add(prevMove.day, new ArrayList<Move>());
			moves.get(1).add(prevMove.day, new ArrayList<Move>());
			
			for (int j = 0; j < SDMatrix[prevMove.day].length; j++) // pre jednu sestru
			{
				for (int k = j + 1; k < SDMatrix[prevMove.day].length; k++) // prejst vsetky sestry
				{
					//if (j == k) continue; // sama so sebou
					if (SDMatrix[prevMove.day][j] != null) // sestra 1 ma sluzbu
					{
						if (SDMatrix[prevMove.day][k] != null && !(SDMatrix[prevMove.day][k].ID.equals(SDMatrix[prevMove.day][j].ID))) // sestra 2 ma sluzbu a nie tu istu
						{
							moves.get(1).get(prevMove.day).add(new Move(j, SDMatrix[prevMove.day][j].index, prevMove.day, k, SDMatrix[prevMove.day][k].index)); // move 2
						}
						else if (SDMatrix[prevMove.day][k] == null) // sestra 2 nema sluzbu -> move 1
						{
							moves.get(0).get(prevMove.day).add(new Move(j, SDMatrix[prevMove.day][j].index, prevMove.day, k, -1));
						}
					}
					else // sestra 1 nema sluzbu
					{
						if (SDMatrix[prevMove.day][k] != null) // sestra 2 ma
						{
							moves.get(0).get(prevMove.day).add(new Move(k, SDMatrix[prevMove.day][k].index, prevMove.day, j, -1));
						}
					}
				}
			}
		}
		
		return moves;
			
	}


	/************** INICIALIALNE ZISKANIE USKUTOCNITELNYCH TAHOV *****************/
	private List<List<List<Move>>> getFeasibleMoves()
	{
		List<List<List<Move>>> moves = new ArrayList<List<List<Move>>>();
		moves.add(new ArrayList<List<Move>>());
		moves.add(new ArrayList<List<Move>>());
		
		for (int i = 0; i < SDMatrix.length; i++) // den
		{
			moves.get(0).add(new ArrayList<Move>());
			moves.get(1).add(new ArrayList<Move>());
			for (int j = 0; j < SDMatrix[i].length; j++) // pre jednu sestru
			{
				for (int k = j + 1; k < SDMatrix[i].length; k++) // prejst vsetky sestry
				{
					//if (j == k) continue; // sama so sebou
					if (SDMatrix[i][j] != null) // sestra 1 ma sluzbu
					{
						if (SDMatrix[i][k] != null && !(SDMatrix[i][k].ID.equals(SDMatrix[i][j].ID))) // sestra 2 ma sluzbu a nie tu istu
						{
							moves.get(1).get(i).add(new Move(j, SDMatrix[i][j].index, i, k, SDMatrix[i][k].index)); // move 2
						}
						else if (SDMatrix[i][k] == null) // sestra 2 nema sluzbu -> move 1
						{
							moves.get(0).get(i).add(new Move(j, SDMatrix[i][j].index, i, k, -1));
						}
					}
					else // sestra 1 nema sluzbu
					{
						if (SDMatrix[i][k] != null) // sestra 2 ma
						{
							moves.get(0).get(i).add(new Move(k, SDMatrix[i][k].index, i, j, -1));
						}
					}
				}
			}
		}
		
		
		return moves;
	}
	

	
	
}

class Move
{
	public int nurse1;
	public int shift1;
	public int day;
	public int nurse2;
	public int shift2;
	
	public Move(int nurse1, int shift1, int day, int nurse2, int shift2)
	{
		this.nurse1 = nurse1;
		this.shift1 = shift1;
		this.day = day;
		this.nurse2 = nurse2;
		this.shift2 = shift2;
	}
};


