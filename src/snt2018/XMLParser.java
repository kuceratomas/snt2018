package snt2018;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public final class XMLParser
{
	
	/************** PARSOVANIE VSTUPNEHO XML *****************/
	public static XMLData parse(String fileName) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException
	{
		XMLData xmlData = new XMLData();
		
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    File file = new File(fileName);
	    if(!file.exists() || file.isDirectory()) { 
	    	System.err.println("Wrong file name!");
	    	System.exit(1);
	    }
	    
	    Document doc = builder.parse(file);
	    doc.getDocumentElement().normalize();
	    XPath xpath = XPathFactory.newInstance().newXPath();
	    
	    // START/END DATE
	    StartEndDate seDate = new StartEndDate();
	    XPathExpression expr1 = xpath.compile("/SchedulingPeriod/StartDate");
        NodeList nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        seDate.startDate = nodes.item(0).getFirstChild().getNodeValue();
        expr1 = xpath.compile("/SchedulingPeriod/EndDate");
        nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        seDate.endDate = nodes.item(0).getFirstChild().getNodeValue();
        xmlData.seDate = seDate;
	    
	    
	    // SKILLS
        expr1 = xpath.compile("/SchedulingPeriod/Skills/Skill");
        nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++)
        {
            Node node = nodes.item(i);
            xmlData.skills.add(node.getFirstChild().getNodeValue());
        }
        
        // SHIFTS
        expr1 = xpath.compile("/SchedulingPeriod/ShiftTypes/Shift");
        nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++)
        {
        	Shift shift = new Shift();
        	Node node = nodes.item(i);
        	shift.index = i;
        	shift.ID = node.getAttributes().getNamedItem("ID").getNodeValue();
        	shift.sTime = node.getChildNodes().item(1).getFirstChild().getNodeValue();
        	shift.eTime = node.getChildNodes().item(3).getFirstChild().getNodeValue();
        	shift.desc = node.getChildNodes().item(5).getFirstChild().getNodeValue();
        	
        	NodeList nodes2 = node.getChildNodes().item(7).getChildNodes();
        	for (int j = 0; j < nodes2.getLength(); j++)
            {
                Node node2 = nodes2.item(j);
                if (node2.getNodeType() == Node.ELEMENT_NODE)
                	shift.skills.add(node2.getFirstChild().getNodeValue());
            }
        	xmlData.shifts.add(shift);
        }
        
        // PATTERNS
        expr1 = xpath.compile("/SchedulingPeriod/Patterns/Pattern");
        nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++)
        {
        	Pattern pattern = new Pattern();
        	Node node = nodes.item(i);
        	pattern.ID = node.getAttributes().getNamedItem("ID").getNodeValue();
        	pattern.weight = Integer.parseInt(node.getAttributes().getNamedItem("weight").getNodeValue());
        	
        	NodeList nodes2 = node.getChildNodes().item(1).getChildNodes();
            for (int j = 0; j < nodes2.getLength(); j++)
            {
                Node node2 = nodes2.item(j);
                if (node2.getNodeType() == Node.ELEMENT_NODE)
                {
                	PatternEntry pEntry = new PatternEntry();
                	pEntry.sType = node2.getChildNodes().item(1).getFirstChild().getNodeValue();
                	String day = node2.getChildNodes().item(3).getFirstChild().getNodeValue();
                	
                	switch(day.substring(0, 2))
                	{
                		case "Mo": pEntry.day = 0; break;
                		case "Tu": pEntry.day = 1; break;
                		case "We": pEntry.day = 2; break;
                		case "Th": pEntry.day = 3; break;
                		case "Fr": pEntry.day = 4; break;
                		case "Sa": pEntry.day = 5; break;
                		case "Su": pEntry.day = 6; break;
                		default: pEntry.day = 7; break; // ANY
                		
                	}
                	pattern.pEntries.add(pEntry);
                }
            }
            xmlData.patterns.add(pattern);
        }
        
        //CONTRACTS
        expr1 = xpath.compile("/SchedulingPeriod/Contracts/Contract");
        nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++)
        {
        	Contract contract = new Contract();
        	Node node = nodes.item(i);
        	contract.ID = node.getAttributes().getNamedItem("ID").getNodeValue();
        	NodeList nodes2 = node.getChildNodes();
        	for (int j = 2; j < nodes2.getLength(); j++)
        	{
        		Node node2 = nodes2.item(j);
        		if (node2.getNodeType() == Node.ELEMENT_NODE && !node2.getNodeName().equals("UnwantedPatterns"))
                {
	        		SoftConstr softConstr = new SoftConstr();
	        		if (hasAttribute(node2, "on"))
	        			softConstr.on = Integer.parseInt(node2.getAttributes().getNamedItem("on").getNodeValue());
	        		if (hasAttribute(node2, "weight"))
	        			softConstr.weight = Integer.parseInt(node2.getAttributes().getNamedItem("weight").getNodeValue());
	
	        		
	        		String value = node2.getFirstChild().getNodeValue();
        			try { softConstr.value = Integer.parseInt(value); }
        			catch (NumberFormatException nfe) 
        			{
        				if (value.equals("true")) softConstr.value = 1;
    	        		else if(value.equals("false")) softConstr.value = 0;
    	        		else if (value.equals("SaturdaySunday")) softConstr.value = 0;
        				else if (value.equals("FridaySaturdaySunday")) softConstr.value = 1;
        				else if (value.equals("FridaySaturdaySundayMonday")) softConstr.value = 2;
        				else softConstr.value = 3;
        			}
	        		
	        		contract.constr.add(softConstr);

                }
        		else if (node2.getNodeType() == Node.ELEMENT_NODE && node2.getNodeName().equals("UnwantedPatterns")) // unwanted patterns
        		{
        			NodeList nodes3 = node2.getChildNodes();
        			for (int k = 0; k < nodes3.getLength(); k++)
                    {
                        Node node3 = nodes3.item(k);
                        if (node3.getNodeType() == Node.ELEMENT_NODE)
                        {
                        	String pattern = node3.getFirstChild().getNodeValue();
                        	for (Pattern p : xmlData.patterns) // vyhladanie kontraktu s rovnakym ID
                				if (p.ID.equals(pattern))
                					contract.uPatterns.add(p);
                        	
                        }
                        	
                        
                    }
        		}

        	}
        	xmlData.contracts.add(contract);
        }
        
        //EMPLOYEES
        expr1 = xpath.compile("/SchedulingPeriod/Employees/Employee");
        nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++)
        {
        	Employee employee = new Employee();
        	Node node = nodes.item(i);
        	employee.index = i;
        	employee.ID = node.getAttributes().getNamedItem("ID").getNodeValue();
        	
        	String contract = node.getChildNodes().item(1).getFirstChild().getNodeValue();
        	for (Contract c : xmlData.contracts) // vyhladanie kontraktu s rovnakym ID
				if (c.ID.equals(contract))
				{
					employee.contract = c;
					if (c.constr.get(PriceEvaluator.Con.WDef.getI()).value == 0) employee.weekend = Arrays.asList(5, 6);
					else if (c.constr.get(PriceEvaluator.Con.WDef.getI()).value == 1) employee.weekend = Arrays.asList(4, 5, 6);
					else employee.weekend = Arrays.asList(5, 6, 0);

				}
					
        	
        	
        	
        	NodeList nodes2 = node.getChildNodes().item(5).getChildNodes();
            for (int j = 0; j < nodes2.getLength(); j++) {
                Node node2 = nodes2.item(j);
                if (node2.getNodeType() == Node.ELEMENT_NODE)
                	employee.skills.add(node2.getFirstChild().getNodeValue());

            }
            xmlData.employees.add(employee);
        }
        
        //COVER REQIUREMENTS
        expr1 = xpath.compile("/SchedulingPeriod/CoverRequirements/DayOfWeekCover"); // alebo date specific cover???
        nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++)
        {
        	DayOfWeekCover dowCover = new DayOfWeekCover();
        	Node node = nodes.item(i);
        	String day = node.getChildNodes().item(1).getFirstChild().getNodeValue();
        	
        	switch(day.substring(0, 2))
        	{
        		case "Mo": dowCover.day = 0; break;
        		case "Tu": dowCover.day = 1; break;
        		case "We": dowCover.day = 2; break;
        		case "Th": dowCover.day = 3; break;
        		case "Fr": dowCover.day = 4; break;
        		case "Sa": dowCover.day = 5; break;
        		default: dowCover.day = 6; break;
        		
        	}
        	
        	NodeList nodes2 = node.getChildNodes(); // cover
        	for (int j = 2; j < nodes2.getLength(); j++)
            {
        		Node node2 = nodes2.item(j);
        		if (node2.getNodeType() == Node.ELEMENT_NODE)
        		{
        			Cover cover = new Cover();
        			String shift = node2.getChildNodes().item(1).getFirstChild().getNodeValue();
        			for (Shift s : xmlData.shifts) // vyhladanie zmeny s rovnakym ID
        				if (s.ID.equals(shift))
        					cover.shift = s;
        					
        			cover.preferred = Integer.parseInt(node2.getChildNodes().item(3).getFirstChild().getNodeValue());
        			dowCover.covers.add(cover);
                }
            }
        	xmlData.dowCovers.add(dowCover);
        }
        
        // DAYS OFF
        expr1 = xpath.compile("/SchedulingPeriod/DayOffRequests/DayOff"); // alebo date specific cover???
        nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++)
        {
        	DayOff dayOff = new DayOff();
        	Node node = nodes.item(i);
        	dayOff.weight = Integer.parseInt(node.getAttributes().getNamedItem("weight").getNodeValue());
        	String ID = node.getChildNodes().item(1).getFirstChild().getNodeValue();
        	dayOff.date = node.getChildNodes().item(3).getFirstChild().getNodeValue();
        	
        	for (Employee e : xmlData.employees) // vyhladanie employee s rovnakym ID
				if (e.ID.equals(ID))
					e.daysOff.put(dayOff.date, dayOff);
        	
        	//xmlData.daysOff.add(dayOff);
        }
        
     // SHIFTS OFF
        expr1 = xpath.compile("/SchedulingPeriod/ShiftOffRequests/ShiftOff"); // alebo date specific cover???
        nodes = (NodeList)expr1.evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++)
        {
        	ShiftOff shiftOff = new ShiftOff();
        	Node node = nodes.item(i);
        	shiftOff.weight = Integer.parseInt(node.getAttributes().getNamedItem("weight").getNodeValue());
        	shiftOff.sType = node.getChildNodes().item(1).getFirstChild().getNodeValue();
        	String ID = node.getChildNodes().item(3).getFirstChild().getNodeValue();
        	shiftOff.date = node.getChildNodes().item(5).getFirstChild().getNodeValue();
        	
        	for (Employee e : xmlData.employees) // vyhladanie employee s rovnakym ID
				if (e.ID.equals(ID))
					e.shiftsOff.put(shiftOff.sType + shiftOff.date, shiftOff);
        	
        	//xmlData.shiftsOff.add(shiftOff);
        }
        
	    return xmlData;
	}
	
	public static boolean hasAttribute(Node element, String value) {
	    NamedNodeMap attributes = element.getAttributes();
	    if (attributes == null) return false;
	    for (int i = 0; i < attributes.getLength(); i++) {
	        Node node = attributes.item(i);
	        if (value.equals(node.getNodeName())) {
	            return true;
	        }
	    }
	    return false;
	}

	
}

/************** TRIEDY PRE ULOZENIE VSTUPNYCH DAT *****************/
class XMLData
{
	public StartEndDate seDate;
    public List<String> skills = new ArrayList<String>(); 
    public List<Shift> shifts = new ArrayList<Shift>();  
    public List<Pattern> patterns = new ArrayList<Pattern>();
    public List<Contract> contracts = new ArrayList<Contract>(); 
    public List<Employee> employees = new ArrayList<Employee>(); 
    public List<DayOfWeekCover> dowCovers = new ArrayList<DayOfWeekCover>();
    //public ArrayList<DayOff> daysOff = new ArrayList<DayOff>();
    //public ArrayList<ShiftOff> shiftsOff = new ArrayList<ShiftOff>(); 
};

class StartEndDate
{
	public String startDate;
	public String endDate;
};
 
class Shift implements Cloneable
{
	int index;
	public String ID;
	public String sTime; 
	public String eTime;  
	public String desc;
	public List<String> skills = new ArrayList<String>();
	
	@Override
	public Shift clone() throws CloneNotSupportedException {
	    return (Shift)super.clone();
	}
};
  
class Pattern
{
	public String ID;
	public int weight; 
	public List<PatternEntry> pEntries = new ArrayList<PatternEntry>(); 
};

class PatternEntry
{
	public String sType;  
	public int day;
};

class Contract
{
	public String ID;
	public List<SoftConstr> constr = new ArrayList<SoftConstr>(); 
	public List<Pattern> uPatterns = new ArrayList<Pattern>(); 
};

class SoftConstr
{
	public int on;
	public int weight;
	public int value;
};

class Employee implements Cloneable
{
	public int index;
	public String ID;
	public Contract contract;
	public String name;
	public List<String> skills = new ArrayList<String>();
	//public List<DayOff> daysOff = new ArrayList<DayOff>();
	//public List<ShiftOff> shiftsOff = new ArrayList<ShiftOff>();
	public List<Integer> weekend;
	public Map<String, DayOff> daysOff = new HashMap<String, DayOff>();
	public Map<String, ShiftOff> shiftsOff = new HashMap<String, ShiftOff>();
};

class DayOfWeekCover implements Cloneable
{
	public int day;
	public int dayFromDate = 0;
	public String date;
	public ArrayList<Cover> covers = new ArrayList<Cover>();
	
	@SuppressWarnings("unchecked")
	@Override
	public DayOfWeekCover clone() throws CloneNotSupportedException
	{
		DayOfWeekCover x = (DayOfWeekCover) super.clone();
		x.covers = (ArrayList<Cover>) this.covers.clone();
		for(int i = 0; i < covers.size(); i++)
		{
			x.covers.remove(i);
			x.covers.add(i, covers.get(i).clone()); 
		}
		
	    return (DayOfWeekCover)x;
	}
};

class Cover implements Cloneable
{
	public Shift shift;
	public int preferred;
	
	@Override
	public Cover clone() throws CloneNotSupportedException
	{
		Cover x = (Cover) super.clone();
		x.shift = (Shift)this.shift.clone();
	    return (Cover)x;
	}
};

class DayOff
{
	public int weight;
	public String date;
};

class ShiftOff
{
	public int weight;
	public String sType;
	public String date;
};




